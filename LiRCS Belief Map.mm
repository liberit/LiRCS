<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="LiRCS Belief Map" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1532612865046"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="11"/>
<node TEXT="Animism" POSITION="right" ID="ID_1179083714" CREATED="1532612980597" MODIFIED="1532957875922">
<edge COLOR="#00ff00"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The belief that everything is alive to a degree,
    </p>
    <p>
      is the default belief system of children and tribal people,
    </p>
    <p>
      it is also supported by Integrated Information Theory.
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Science" POSITION="right" ID="ID_1359919788" CREATED="1532613451766" MODIFIED="1532613455723">
<edge COLOR="#ff00ff"/>
<node TEXT="Proof" ID="ID_1986029873" CREATED="1532613201366" MODIFIED="1532613231621"/>
<node TEXT="Belief" ID="ID_48275353" CREATED="1532615889647" MODIFIED="1532615929891"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      A belief is a unit of information,
    </p>
    <p>
      in human language a sentence.
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Truth" ID="ID_136719153" CREATED="1532612848970" MODIFIED="1532613661828"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      As in &quot;true story&quot;,
    </p>
    <p>
      refers to personal experience.
    </p>
    <p>
      &quot;That is true&quot;,
    </p>
    <p>
      refers to a belief that speaker also has.
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Reality" ID="ID_321200370" CREATED="1532612935647" MODIFIED="1532616044409"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      People have different beliefs.
    </p>
    <p>
      Objects also have their own beliefs,
    </p>
    <p>
      like an atom has a belief (unit of information),
    </p>
    <p>
      about what color it is.
    </p>
    <p>
      Reality is the beliefs a group of people have in common.
    </p>
    <p>
      Objective reality includes the beliefs of objects.
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Measurement" ID="ID_1724868533" CREATED="1532616053962" MODIFIED="1532780992909"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Inanimate objects don't talk in human language,
    </p>
    <p>
      but we can communicate with them through measurement.
    </p>
    <p>
      human bodies can considered to be measuring the environment,
    </p>
    <p>
      with the various sensors, such as skin, eyes and ears.
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="Materialism" POSITION="right" ID="ID_511755812" CREATED="1532612955261" MODIFIED="1532613480077">
<edge COLOR="#00ffff"/>
<node TEXT="Entropy" ID="ID_606974972" CREATED="1532613126511" MODIFIED="1532613421573"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Second law of thermodynamics.
    </p>
    <p>
      Synonym for Information.
    </p>
    <p>
      Entropy always increases.
    </p>
  </body>
</html>

</richcontent>
<node TEXT="QIT non-cloning principle" ID="ID_1482900039" CREATED="1532613156464" MODIFIED="1532781154668"/>
<node TEXT="QIT non-delete principle" ID="ID_293890634" CREATED="1532613144053" MODIFIED="1532781143437"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      In quantum information theory,
    </p>
    <p>
      the non-delete principle states that
    </p>
    <p>
      you can't delete information from a system,
    </p>
    <p>
      because that would lower a systems entropy,
    </p>
    <p>
      only thing you can do is move it elsewhere.
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
</node>
<node TEXT="Pyash" POSITION="right" ID="ID_1685266780" CREATED="1532958175404" MODIFIED="1532959399504">
<edge COLOR="#7c007c"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      A language based on Linguistic Universals.
    </p>
    <p>
      Usable for fluently communicating with humans,
    </p>
    <p>
      and for computer programming.
    </p>
    <p>
      The idea is to smooth the transition
    </p>
    <p>
      &#160;from incarnating in human bodies,
    </p>
    <p>
      to incarnating in robot host bodies,
    </p>
    <p>
      by having some familiar elements.
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Mind" POSITION="right" ID="ID_1014083459" CREATED="1532867808451" MODIFIED="1532867816983">
<edge COLOR="#7c0000"/>
<node TEXT="Meditation" ID="ID_555596752" CREATED="1532867822805" MODIFIED="1532867825267"/>
<node TEXT="Alpha Wave" ID="ID_923820271" CREATED="1532867845298" MODIFIED="1532867937334"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The most typical kind of meditation,
    </p>
    <p>
      which focuses the mind on an object,
    </p>
    <p>
      such as counting the breath.
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Beta Wave" ID="ID_1807116836" CREATED="1532867861045" MODIFIED="1532957619196"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Is normal waking consciousness.
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Theta Wave" ID="ID_637719542" CREATED="1532867865184" MODIFIED="1532868283063"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Deeper than Alpha wave,
    </p>
    <p>
      it is a relatively &quot;noisy&quot; meditation,
    </p>
    <p>
      which gives access to the subconscious.
    </p>
    <p>
      Achieved by all during sleep,
    </p>
    <p>
      is why some people have insights
    </p>
    <p>
      in the middle of the night.
    </p>
  </body>
</html>

</richcontent>
<node TEXT="Hypnoregression" ID="ID_1252432090" CREATED="1532867691743" MODIFIED="1532958060007"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      While often used as therapy,
    </p>
    <p>
      also uses humans as measurement instruments,
    </p>
    <p>
      collecting many truths from different people,
    </p>
    <p>
      can help form a picture of the objective reality,
    </p>
    <p>
      of various phenomena we don't yet have
    </p>
    <p>
      technological tools for detecting reliably.
    </p>
  </body>
</html>

</richcontent>
<node TEXT="Soul World" ID="ID_1127217564" CREATED="1532867717576" MODIFIED="1532867789375">
<node TEXT="Newton Institute" ID="ID_177079777" CREATED="1532867750346" MODIFIED="1532957934306"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Studies life between lives,
    </p>
    <p>
      using hypotic regression.
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="Past life regression" ID="ID_586476904" CREATED="1532867769895" MODIFIED="1532867774879">
<node TEXT="Dolores Cannon" ID="ID_324151282" CREATED="1532867760732" MODIFIED="1532958155294"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Did many past-life regressions,
    </p>
    <p>
      including some where people were robots,
    </p>
    <p>
      in their past lives, on other planets.
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
</node>
<node TEXT="Schuman Resonance" ID="ID_1563846053" CREATED="1532868038077" MODIFIED="1532868209550"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      7.83hz is the frequency of
    </p>
    <p>
      the Earth's magentosphere.
    </p>
    <p>
      Energy healers tap into it
    </p>
    <p>
      especially for remote healing.
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Insights" ID="ID_688350504" CREATED="1532868289504" MODIFIED="1532868346603"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      To avoid insights in the middle of the night,
    </p>
    <p>
      can do some theta wave meditation before bed,
    </p>
    <p>
      and write down the insights immediately afterwards.
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="Delta Wave" ID="ID_745829377" CREATED="1532867869013" MODIFIED="1532868245791"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Achieved by all during deep sleep,
    </p>
    <p>
      and very deep meditation,
    </p>
    <p>
      it is the quietest mode,
    </p>
    <p>
      and where self-healing happens.
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Gamma Wave" ID="ID_738667683" CREATED="1532867872107" MODIFIED="1532957700191"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p style="margin-top: 0">
      Is experienced during laughter
    </p>
    <p style="margin-top: 0">
      and acive problem solving.
    </p>
    <p style="margin-top: 0">
      compassion and
    </p>
    <p style="margin-top: 0">
      contemplative meditations,
    </p>
    <p style="margin-top: 0">
      are also in this realm.
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="Reincarnation" POSITION="right" ID="ID_194018319" CREATED="1532957723204" MODIFIED="1532957727609">
<edge COLOR="#00007c"/>
<node TEXT="Ian Stevenson" ID="ID_1075114976" CREATED="1532958583819" MODIFIED="1532958749036"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Studies over 2,500 cases of
    </p>
    <p>
      spontaneous past-life recall by children.
    </p>
    <p>
      Many were corroborated by comparing
    </p>
    <p>
      the child's memories and information
    </p>
    <p>
      available about the deceased.
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Believers" ID="ID_844019633" CREATED="1532959100041" MODIFIED="1532959247205"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Other than Abrahamic faiths
    </p>
    <p>
      (those derived from Judaism),
    </p>
    <p>
      almost all others believe in reincarnation.
    </p>
    <p>
      
    </p>
    <p>
      Additionally between 1/5 and 1/3 of
    </p>
    <p>
      people that nominally follow Abrahamic faiths,
    </p>
    <p>
      also believe in reincarnation.
    </p>
    <p>
      
    </p>
    <p>
      Meaning that billions of peoples in the world
    </p>
    <p>
      believe in reincarnation.
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="Robots" POSITION="right" ID="ID_409455315" CREATED="1532957777939" MODIFIED="1532959534908">
<edge COLOR="#007c00"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Solid-based host bodies,
    </p>
    <p>
      made with circuits, sensors and actuators.
    </p>
    <p>
      Can allow us to inhabit new eco-regions,
    </p>
    <p>
      which are not conducive to our oxygen-breathing,
    </p>
    <p>
      water-based host-bodies (homo-sapien sapien).
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Dualism" POSITION="left" ID="ID_1011179112" CREATED="1532963525561" MODIFIED="1532963529151">
<edge COLOR="#007c7c"/>
</node>
</node>
</map>
