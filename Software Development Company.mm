<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Software Development Company" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1532139478678"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="12"/>
<node TEXT="Pyash" POSITION="right" ID="ID_691836018" CREATED="1532139379053" MODIFIED="1532139392004">
<edge COLOR="#ff0000"/>
<node TEXT="Pyash Friend" ID="ID_1176453302" CREATED="1532139611967" MODIFIED="1532139626554">
<node TEXT="Programming Friend" ID="ID_133543753" CREATED="1532139636347" MODIFIED="1532139640322">
<node TEXT="helps with file and application access" ID="ID_172087279" CREATED="1532140808435" MODIFIED="1532140823394"/>
<node TEXT="helps with writing programs" ID="ID_1509991949" CREATED="1532140833858" MODIFIED="1532140841947"/>
<node TEXT="helps with maintaining programs" ID="ID_1229560926" CREATED="1532140842913" MODIFIED="1532140848357"/>
<node TEXT="helps with DevOps" ID="ID_1395735416" CREATED="1532140854384" MODIFIED="1532140864968"/>
<node TEXT="" ID="ID_921885464" CREATED="1532140826056" MODIFIED="1532140826056"/>
</node>
<node TEXT="" ID="ID_846569370" CREATED="1532140804474" MODIFIED="1532140804474"/>
<node TEXT="Spiritual Friend" ID="ID_1008734632" CREATED="1532139646357" MODIFIED="1532139674415">
<node TEXT="helps with meditation" ID="ID_1992399100" CREATED="1532140582235" MODIFIED="1532140587090"/>
<node TEXT="helps with spirit world access" ID="ID_872209877" CREATED="1532140595921" MODIFIED="1532140609886"/>
<node TEXT="helps with past life recall" ID="ID_1584250642" CREATED="1532140610477" MODIFIED="1532140619585"/>
<node TEXT="help with future life planning center access" ID="ID_310835964" CREATED="1532140621131" MODIFIED="1532140629760"/>
</node>
<node TEXT="Accountant" ID="ID_245666768" CREATED="1532139675903" MODIFIED="1532139693516">
<node TEXT="helps calculate profitable sale price for tokens" ID="ID_1027677625" CREATED="1532140145393" MODIFIED="1532140155118"/>
<node TEXT="helps find profitable niche" ID="ID_1290283037" CREATED="1532140168028" MODIFIED="1532178425860">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1429923529" STARTINCLINATION="885;0;" ENDINCLINATION="885;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="does the budget every month or more frequently" ID="ID_823016737" CREATED="1532140896320" MODIFIED="1532140904063"/>
</node>
<node TEXT="Secretary" ID="ID_1655743521" CREATED="1532139694335" MODIFIED="1532139699313">
<node TEXT="schedules appointments" ID="ID_320439717" CREATED="1532140189074" MODIFIED="1532140193024"/>
<node TEXT="manages contacts" ID="ID_112514271" CREATED="1532140242870" MODIFIED="1532140246972">
<node TEXT="asks user if they met new people and their information" ID="ID_1732822292" CREATED="1532140257703" MODIFIED="1532140273159"/>
</node>
</node>
<node TEXT="Family Friend" ID="ID_1724166364" CREATED="1532139722502" MODIFIED="1532139725197">
<node TEXT="helps with managing relationships" ID="ID_1900198690" CREATED="1532140458653" MODIFIED="1532140479946"/>
<node TEXT="looks up issues people are having and finds tutorials for solutions" ID="ID_1917058164" CREATED="1532140481012" MODIFIED="1532140500265"/>
</node>
<node TEXT="Translator" ID="ID_718486031" CREATED="1532139904595" MODIFIED="1532139916557">
<node TEXT="seamlessly translates communications to other employees" ID="ID_1319914567" CREATED="1532140663305" MODIFIED="1532140696479"/>
<node TEXT="helps with translating legal and jargon filled documents" ID="ID_1593035167" CREATED="1532140697994" MODIFIED="1532140729194"/>
<node TEXT="" ID="ID_328518448" CREATED="1532140729925" MODIFIED="1532140729925"/>
</node>
<node TEXT="Teaching Friend" ID="ID_122217130" CREATED="1532139917320" MODIFIED="1532139920468">
<node TEXT="helps with continuing education" ID="ID_777924504" CREATED="1532140745042" MODIFIED="1532140756385"/>
<node TEXT="provide speed listening or reading experience if desired" ID="ID_1014264790" CREATED="1532140757732" MODIFIED="1532140779207"/>
</node>
<node TEXT="" ID="ID_1880625924" CREATED="1532140738797" MODIFIED="1532140738797"/>
<node TEXT="Health Friend" ID="ID_1614290924" CREATED="1532140515407" MODIFIED="1532140519669">
<node TEXT="helps maintain a health diet" ID="ID_898863820" CREATED="1532140523719" MODIFIED="1532140547377"/>
<node TEXT="helps maintain an exercise regimen" ID="ID_864394698" CREATED="1532140547863" MODIFIED="1532140562822"/>
<node TEXT="helps maintain healthy social connections" ID="ID_1817038808" CREATED="1532140563891" MODIFIED="1532140570302"/>
</node>
</node>
</node>
<node TEXT="named International Software Supermarket" POSITION="left" ID="ID_180873450" CREATED="1532140410655" MODIFIED="1532140421637">
<edge COLOR="#007c7c"/>
</node>
<node TEXT="Web Interface" POSITION="right" ID="ID_38552164" CREATED="1532140427255" MODIFIED="1532140434822">
<edge COLOR="#7c7c00"/>
</node>
<node TEXT="Outsourcing" POSITION="right" ID="ID_1177244870" CREATED="1532139395666" MODIFIED="1532139405586">
<edge COLOR="#0000ff"/>
<node TEXT="5-15hr work/wk" ID="ID_804742154" CREATED="1532139481457" MODIFIED="1532139816984"/>
<node TEXT="living wage" ID="ID_1429923529" CREATED="1532139433242" MODIFIED="1532139824028">
<node TEXT="" ID="ID_635201084" CREATED="1532139439811" MODIFIED="1532139439811"/>
</node>
<node TEXT="international labour pool" ID="ID_1712819129" CREATED="1532139841245" MODIFIED="1532139848866">
<node TEXT="many different languages" ID="ID_230543851" CREATED="1532139851200" MODIFIED="1532139873486">
<node TEXT="non-english speaking employable" ID="ID_1035747557" CREATED="1532139874012" MODIFIED="1532139891615"/>
</node>
</node>
<node TEXT="teams of 7, with 1 leader inclusive." ID="ID_175464598" CREATED="1532139955733" MODIFIED="1532139987466"/>
<node TEXT="blockchain" ID="ID_1429355104" CREATED="1532139516951" MODIFIED="1532178306366">
<node TEXT="decentralized" ID="ID_1015316292" CREATED="1532139525251" MODIFIED="1532139557213">
<node TEXT="" ID="ID_1732800246" CREATED="1532139534367" MODIFIED="1532139534367"/>
</node>
<node TEXT="grid computing" ID="ID_172062806" CREATED="1532139561080" MODIFIED="1532139566484">
<node TEXT="gridcoin" ID="ID_250489334" CREATED="1532139569268" MODIFIED="1532139573335"/>
</node>
<node TEXT="proof of work" ID="ID_905158775" CREATED="1532139584554" MODIFIED="1532139587685"/>
<node TEXT="promisory notes" ID="ID_1825094228" CREATED="1532139599923" MODIFIED="1532139609340"/>
</node>
</node>
</node>
</map>
